import cobra
import pandas
import escher
import json
pandas.options.display.max_rows=400
import cobra.test
import libsbml
from cobra import Model, Reaction, Metabolite
from cobra import flux_analysis
#below are FVA components

from cobra.flux_analysis import flux_variability_analysis
from cobra.util.solver import linear_reaction_coefficients

model = cobra.io.read_sbml_model("arabidopsis model.xml")
model.objective = "R35"

#rna degradation
model.reactions.R401.bounds=(4.6e-7,1000)

#protein degradation
model.reactions.R402.bounds=(0.0000677,1000)

#atp mainenance
model.reactions.R400.bounds=(0,0)

##to switch between photosynthetic growth and growth on starch, the code below
##has to be commented out.
##this code is used to test the effect of nadph supplementation in the plastid.
##it can also be modified to test any other metabolite - by changing the target
##from nadph to another metabolite.
##IGNORE the warnings

##photosynthetic growth

##model.reactions.R344.bounds=(0,15)#photon flux
##model.reactions.R345.bounds=(-1000,-0.000158)
##model.reactions.R222.bounds=(-1000,1000) #CO2 uptake
##cyclicConstraints=model.problem.Constraint(.1*model.reactions.R57.flux_expression - model.reactions.R58.flux_expression, lb=0, ub=0)
##model.add_cons_vars(cyclicConstraints) #cyclic electron flow
##photorespirationConstraints=model.problem.Constraint(0.25*model.reactions.R197.flux_expression - model.reactions.R196.flux_expression, lb=0, ub=0)
##model.add_cons_vars(photorespirationConstraints) #photorespiration


##starch growth

model.reactions.R345.bounds = (0,0) #starch importmodel.reactions.R222.bounds = (-1000,1000) #CO2 uptake
model.reactions.R344.bounds=(0,0) #photon flux

h_pl = model.metabolites.get_by_id('h_pl')
nadp_pl = model.metabolites.get_by_id('nadp_pl')
nadph_pl =model.metabolites.get_by_id('nadph_pl')

stringg = 'EX_nadph'
stringg = Reaction(stringg)
stringg.name='temporary reaction'
stringg.lower_bound = 0.0
stringg.upper_bound = 1.0
stringg.add_metabolites({ h_pl:1 , nadp_pl:-1 , nadph_pl:1 })

model.add_reaction(stringg)


modd = model.optimize()

print modd.objective_value
